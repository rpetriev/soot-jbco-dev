/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package decomp;

/**
 *
 * @author VVRIP
 */
public class ShellSortSyncBlock {
    
    int[] a;
    Integer sum = 0;
    final int countThreads = 8;
    Sorter[] mlts = new Sorter[countThreads];
    Thread[] thrs = new Thread[countThreads];
    
    public ShellSortSyncBlock(int[] matr){
        
        if(matr == null){
            throw new IllegalArgumentException("Some array is null!");
        }
        a = matr; 
        
        for(int i = 0; i<countThreads; i++){
            mlts[i] = new Sorter(a, i);
            thrs[i] = new Thread(mlts[i]);      
        }   
    }
    
    public int[] sort() throws InterruptedException{
        
        for(int i = 0; i<countThreads; i++){
            thrs[i].start();
        }  
        for(int i = 0; i<countThreads; i++){
            thrs[i].join();
        }  
        return a;
    }    
    
    //--------------------------------------------------------------------------
    
    public class Sorter implements Runnable{
        
        int[] arr;
        int thread_index;
        
        public Sorter(int[] _a, int _thread_index){
            
            arr = _a; thread_index = _thread_index;
        }
        
        @Override
        public void run() {
            
            int j;
            int step = arr.length / 2;
            while (step > 0)
            {
                for (int i = 0; i < (arr.length - step); i = i + thread_index + 1)
                {   
                    j = i;
                    while ((j >= 0) && (arr[j] > arr[j + step]))
                    {
                        int tmp = arr[j];
                        arr[j] = arr[j + step];
                        arr[j + step] = tmp;
                        synchronized(sum){
                            sum += arr[i];
                            if(sum > 1000000){
                                sum = 0;
                            }
                        }
                        j--; 
                    }
                }
                step = step / 2;
            }
        }       
    }    
}
