/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package decomp;

import java.util.Random;

/**
 *
 * @author VVRIP
 */
public class Entity {
    private StringBuilder name;
    public int countGetsOfName;
    private int syncVal;

    public void rnd(){
        Random r = new Random();
        int i = r.nextInt(100);
        int j = r.nextInt(100);
        if(i > j){
            System.out.println("AAA");
        }
    }
    public StringBuilder getEntityName(){
        synchronized(this){
            countGetsOfName++;
        }
        return name;
    }
    public void setSyncVal(int val){
        synchronized(this){
            syncVal = val;
        }
    }
    public int getSyncVal(){
        return syncVal;
    }
    public synchronized void setEntityName(StringBuilder newName){
        name = newName;
    } 
}
