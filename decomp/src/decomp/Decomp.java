/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package decomp;

import java.util.Random;

/**
 *
 * @author VVRIP
 */
public class Decomp {
    
    private static final String FUUU = "ffasgasfgsdg"; 
    private static final String AAAA = "olololololo";
    private static final int key = 3;
    private static String str;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        
        init();
        
        lsp_test_1();
        
        sync_test_1();
        sync_test_2();
    }
    
    public static void init(){
        str = AAAA;
        String str = "init\n";
        for(int i = 0; i<str.length(); i++){
            System.out.print(str.charAt(i));
        }
    }
    
    public static void lsp_test_1(){
        
        System.out.println(FUUU);
        System.out.println(AAAA);
        System.out.println("crypt = " + crypt(FUUU));
        System.out.println("crypt|encrypt = " + encrypt(crypt(FUUU)));
        System.out.println("agaga");
        System.out.println(str);
        System.out.println(getString1Value());
        System.out.println(getString2Value());
    }
    
    public static void sync_test_1(){
        
        StringBuilder sb = new StringBuilder();
        synchronized(sb){
//            synchronized(str2){
                sb.append("fff");
//                str2.append("fff");
//            }
            if(sb.toString().equalsIgnoreCase("hhh")){
                return;
            }
//        synchronized(str3){
//            str3.append("fff");       
//        }
            Entity entity = new Entity();
            entity.setEntityName(sb);
        }
    }
    
    public static synchronized void forTest()
    {
        System.out.println("fff");
    }
    
    public static void sync_test_2() throws InterruptedException{
        
        int[] arr = getRandMatrix(120000, 0, 100);
        int[] c_arr = new int[arr.length];
        System.arraycopy(arr, 0, c_arr, 0, arr.length);
        int[] d_arr = new int[arr.length];
        System.arraycopy(arr, 0, d_arr, 0, arr.length);
        int[] e_arr = new int[arr.length];
        System.arraycopy(arr, 0, e_arr, 0, arr.length);
        int[] f_arr = new int[arr.length];
        System.arraycopy(arr, 0, f_arr, 0, arr.length);
        int[] g_arr = new int[arr.length];
        System.arraycopy(arr, 0, g_arr, 0, arr.length);
        
        ShellSortSyncBlock ss = new ShellSortSyncBlock(arr);
        long start = System.currentTimeMillis();
        arr = ss.sort();
        long end = System.currentTimeMillis();
        System.out.println("Time parallel SB: " + (end - start));
        
        ShellSortSyncMethod ss1 = new ShellSortSyncMethod(c_arr);
        start = System.currentTimeMillis();
        c_arr = ss1.sort();//stdSort(c_arr);
        end = System.currentTimeMillis();
        System.out.println("Time parallel SM: " + (end - start));
        
        ShellSortOriginal ss2 = new ShellSortOriginal(d_arr);
        start = System.currentTimeMillis();
        d_arr = ss2.sort();
        end = System.currentTimeMillis();
        System.out.println("Time parallel orig: " + (end - start));
        
        start = System.currentTimeMillis();
        e_arr = stdSort(e_arr);
        end = System.currentTimeMillis();
        System.out.println("Time std: " + (end - start));
        
        ShellSortSyncBlockOrig ss3 = new ShellSortSyncBlockOrig(f_arr);
        start = System.currentTimeMillis();
        f_arr = ss3.sort();
        end = System.currentTimeMillis();
        System.out.println("Time parallel SB orig: " + (end - start));
        
        ShellSortSyncMethodOrig ss4 = new ShellSortSyncMethodOrig(g_arr);
        start = System.currentTimeMillis();
        g_arr = ss4.sort();
        end = System.currentTimeMillis();
        System.out.println("Time parallel SM orig: " + (end - start));
        
        //printMatrix(arr);
        //printMatrix(c_arr);
    }
    
    public static String getString1Value() {
        return FUUU;
    }
    public static String getString2Value() {
        return AAAA;
    }
    
    public static String crypt(String s){
        
        StringBuilder str = new StringBuilder(s);
        for(int i = 0; i < str.length(); i++){
            str.setCharAt(i, my_reverseBytes(str.charAt(i)));
            str.setCharAt(i, crypt_char_1(str.charAt(i), str.length()));
        }
        return str.toString();
    }
    
    public static String encrypt(String s){
        
        StringBuilder str = new StringBuilder(s);
        for(int i = 0; i < str.length(); i++){
            str.setCharAt(i, encrypt_char_1(str.charAt(i), str.length()));
            str.setCharAt(i, my_reverseBytes(str.charAt(i)));
        }
        return str.toString();
    }
    
    public static char crypt_char_1(char ch, int str_len){
        
        if(str_len < Character.MAX_VALUE){
            if(ch + str_len < Character.MAX_VALUE){
                ch = (char)(ch + str_len);
            }else{
                if(ch - str_len > 0){
                    ch = (char)(ch - str_len);
                }else{
                    ch = (char)(ch + key);
                }
            }
        }else{
            if(ch + key < Character.MAX_VALUE){
                ch = (char)(ch + key);
            }else{
                if(ch - key > 0){
                    ch = (char)(ch - key);
                }else{
                    ch = (char)(ch + key);
                }
            }
        }
        return ch;
    }
    
    public static char encrypt_char_1(char ch, int str_len){
        
        if(str_len < Character.MAX_VALUE){
            if(ch + str_len <= Character.MAX_VALUE && ch - 2*str_len < 0){
                ch = (char)(ch + str_len);
            }else{
                ch = (char)(ch - str_len);
            }
        }else{
            if(ch + key <= Character.MAX_VALUE && ch - 2*key < 0){
                ch = (char)(ch - key);
            }else{
                ch = (char)(ch + key);
            }
        }
        return ch;
    }
    
    public static char my_reverseBytes(char ch){
        return (char) (((ch & 0xFF00) >> 8) | (ch << 8));
    }
    
    public static int[] getRandMatrix(int n, int start, int end){
        
        if(n < 1){
            throw new IllegalArgumentException("Matrix size is incorrect!");
        }
        int[] a = new int[n];
        Random rnd = new Random();
        for(int i = 0; i<n; i++){
                a[i] = rnd.nextInt(end) + start;
        }
        return a;
    }
    
    public static void printMatrix(int[] a){
        
        for(int i = 0; i<a.length; i++){
            System.out.print(a[i] + " ");
        }
        System.out.println("\n------------");
    }
    
    public static int[] stdSort(int[] arr){
    
        int j;
        int step = arr.length / 2;
        while (step > 0)
        {
            for (int i = 0; i < (arr.length - step); i++)
            {
                j = i;
                while ((j >= 0) && (arr[j] > arr[j + step]))
                {
                    int tmp = arr[j];
                    arr[j] = arr[j + step];
                    arr[j + step] = tmp;
                    j--; 
                }
            }
            step = step / 2;
        }
        return arr;
    }

}
