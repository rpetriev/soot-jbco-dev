/* Soot - a J*va Optimization Framework
 * Copyright (C) 2015 Roman Petriev
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

package soot.jbco.jimpleTransformations;

import java.util.ArrayList;
import java.util.Map;
import soot.Body;
import soot.BodyTransformer;
import soot.CharType;
import soot.IntType;
import soot.Local;
import soot.RefType;
import soot.Scene;
import soot.SootField;
import soot.SootMethodRef;
import soot.Unit;
import soot.Value;
import soot.jbco.IJbcoTransform;
import soot.jbco.util.Rand;
import soot.jimple.*;
import soot.tagkit.StringConstantValueTag;
import soot.tagkit.Tag;

/**
 * @author Roman Petriev 
 * 
 * Created on 15-Feb-2015 
 */
public class LocalStringsProtection extends BodyTransformer implements IJbcoTransform {
    
    public static String dependancies[] = new String[] { "wjtp.jbco_fr", "bb.jbco_ful", "jtp.jbco_lsp" };
    public static String name = "jtp.jbco_lsp";
    private int indexOffset = 0;
    
    private int fieldsProtected = 0;
    private int invokesProtected = 0;
    private int assignStmtsProtected = 0;
    private int returnsProtected = 0;
    
    private static boolean isShowed = false;
    private ArrayList<InvokeStmt> suitableIS;
    private ArrayList<AssignStmt> suitableAS;
    private ArrayList<ReturnStmt> suitableRS;
    
    SootMethodRef sbInitRef;
    SootMethodRef sbCharAtRef;
    SootMethodRef sbSetCharAtRef;
    SootMethodRef sbToStringRef;
    SootMethodRef sbLenghtRef;
    
    private void init(){
        
        suitableIS = new ArrayList<>();
        suitableAS = new ArrayList<>();
        suitableRS = new ArrayList<>();
        
        indexOffset = Rand.getInt(100); 
        
        sbInitRef = Scene.v().getMethod("<java.lang.StringBuilder: void <init>(java.lang.String)>").makeRef();
        sbCharAtRef = Scene.v().getMethod("<java.lang.StringBuilder: char charAt(int)>").makeRef();
        sbSetCharAtRef = Scene.v().getMethod("<java.lang.StringBuilder: void setCharAt(int,char)>").makeRef();
        sbToStringRef = Scene.v().getMethod("<java.lang.StringBuilder: java.lang.String toString()>").makeRef();
        sbLenghtRef = Scene.v().getMethod("<java.lang.StringBuilder: int length()>").makeRef();
    }
    
    @Override
    protected void internalTransform(Body body, String phaseName, Map<String, String> options) {
        
        System.out.println(body.getMethod().getSignature());
        
        int weight = soot.jbco.Main.getWeight(phaseName, body.getMethod().getSignature());
        if (weight == 0){ 
            return;
        }
        
        init();
        
        System.out.println(" --- --- --- --- --- --- ");
        
//        if(body.getMethod().getSignature().contains("encrypt_char_1")){
//            PrintWriter writer = null;
//            try {
//                writer = new PrintWriter("file.txt", "UTF-8");
//            }
//            catch (FileNotFoundException ex) {
//                Logger.getLogger(LocalStringsProtection.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            catch (UnsupportedEncodingException ex) {
//                Logger.getLogger(LocalStringsProtection.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            for(Unit u : body.getUnits()){
//                writer.println(u.toString() + " --- " + u.getClass());
//            }
//            writer.close();
//        }
        
        collectSuitableStmts(body);
        
        obfuscateInvokeStmts(body);
        obfuscateAssignStmts(body);
        obfuscateReturnStmts(body);
        
        if(!isShowed){
            isShowed = true;

            System.out.println("------------------------------------");
            for(SootField field : body.getMethod().getDeclaringClass().getFields()){
                System.out.println("Field : " + field.getName() + ", Value : " + field.toString());

                System.out.println(field.getTags());
                for(int i = 0; i<field.getTags().size(); i++){
                    Tag t = field.getTags().get(i);
                    if(t instanceof StringConstantValueTag){ 
                        fieldsProtected++;
                        field.getTags().remove(t);
                        field.getTags().add(new StringConstantValueTag(generateString(((StringConstantValueTag)t).getStringValue())));
                        break;
                    }
                }
                
            }
        }
        System.out.println(" --- --- --- --- --- --- ");
    }

    @Override
    public void outputSummary() {
        out.println("Fields protected: " + fieldsProtected);
        out.println("Method calls protected: " + invokesProtected);
        out.println("Assign statements protected: " + assignStmtsProtected);
        out.println("Return statements protected: " + returnsProtected);
    }

    @Override
    public String[] getDependancies() {
        return dependancies;
    }

    @Override
    public String getName() {
        return name;
    }
    
    private String generateString(String realValue){
        
        if(realValue.length() > 1){
            StringBuilder sb = new StringBuilder();
            int len = realValue.length()/2 + Rand.getInt(realValue.length()/2 - 1);
            for(int i = 0; i<len; i++){
                sb.append((char)Rand.getInt(realValue.charAt(i)));
            }
            return sb.toString();
        }else{
            return "";
        }
    }
    
    private void collectSuitableStmts(Body body){
        
        System.out.println(" Current method " + body.getMethod().getSignature());
        
        for(Unit u : body.getUnits()){
            
            if(u instanceof InvokeStmt){
                InvokeStmt is = (InvokeStmt)u;
                InvokeExpr ie = (InvokeExpr)is.getInvokeExpr();

                if(ie.getArgs().size() > 0){
                    for(Value v : ie.getArgs()){
                        if(v instanceof StringConstant){
                            suitableIS.add(is);
                            break;
                        }
                    }
                }
            }
            
            if(u instanceof AssignStmt){
                AssignStmt as = (AssignStmt)u;

                if(as.getRightOp() instanceof StringConstant){
                    suitableAS.add(as);
                }
            }
            
            if(u instanceof ReturnStmt){
                ReturnStmt retutnStmt = (ReturnStmt)u;

                if(retutnStmt.getOp() instanceof StringConstant){
                    suitableRS.add(retutnStmt);
                }
            }
        }
    }
    
    private void obfuscateReturnStmts(Body body){
        int localIndex = 0;
        for(int i = 0; i<suitableRS.size(); i++){
            ReturnStmt returnStmt = suitableRS.get(i);
            if(returnStmt.getOp() instanceof StringConstant){
                StringConstant sc = (StringConstant)returnStmt.getOp();
                returnStmt.setOp(generateEncryptMethod(body, returnStmt, sc, localIndex, "lcs_for_rs_"));
                localIndex++;
                returnsProtected++;
            }
        }
    }
    
    private void obfuscateAssignStmts(Body body){
        int localIndex = 0;
        for(int i = 0; i<suitableAS.size(); i++){
            AssignStmt as = suitableAS.get(i);
            if(as.getRightOp() instanceof StringConstant){
                StringConstant sc = (StringConstant)as.getRightOp();
                as.setRightOp(generateEncryptMethod(body, as, sc, localIndex, "lcs_for_as_"));
                localIndex++;
                assignStmtsProtected++;
            }
        }
    }
    
    private void obfuscateInvokeStmts(Body body){
        
        int localIndex = 0;
        for(int i = 0; i<suitableIS.size(); i++){
            InvokeStmt is = suitableIS.get(i);
            InvokeExpr ie = is.getInvokeExpr();
            ArrayList<Value> newArgs = new ArrayList<>();
            invokesProtected++;
            if(ie instanceof VirtualInvokeExpr){
                VirtualInvokeExpr virtualIE = (VirtualInvokeExpr)ie;
                for(Value v : virtualIE.getArgs()){
                    if(v instanceof StringConstant){
                        StringConstant sc = (StringConstant)v;
                        System.out.println(" --- VirtualIS Value SC " + sc.value);
                        newArgs.add(generateEncryptMethod(body, is, sc, localIndex, "lcs_for_v_ie_"));
                        ++localIndex;
                    }else{
                        newArgs.add(v);
                    }
                }
                VirtualInvokeExpr newVirtualIE = Jimple.v().newVirtualInvokeExpr(
                        (Local)virtualIE.getBase(), 
                        virtualIE.getMethod().makeRef(), 
                        newArgs);
                is.setInvokeExpr(newVirtualIE);
                System.out.println(" --- New VirtualIE " + newVirtualIE.toString());
            }
            
            if(ie instanceof StaticInvokeExpr){
                StaticInvokeExpr staticIE = (StaticInvokeExpr)ie;
                for(Value v : staticIE.getArgs()){
                    if(v instanceof StringConstant){
                        StringConstant sc = (StringConstant)v;
                        System.out.println(" --- StaticIS Value SC " + sc.value);
                        newArgs.add(generateEncryptMethod(body, is, sc, localIndex, "lcs_for_st_ie_"));
                        ++localIndex;
                    }else{
                        newArgs.add(v);
                    }
                }
                StaticInvokeExpr newStaticIE = Jimple.v().newStaticInvokeExpr(
                        staticIE.getMethod().makeRef(), 
                        newArgs);
                is.setInvokeExpr(newStaticIE);
                System.out.println(" --- New StaticIE " + newStaticIE.toString());
            }
            
            if(ie instanceof SpecialInvokeExpr){
                SpecialInvokeExpr specialIE = (SpecialInvokeExpr)ie;
                for(Value v : specialIE.getArgs()){
                    if(v instanceof StringConstant){
                        StringConstant sc = (StringConstant)v;
                        System.out.println(" --- SpecialIS Value SC " + sc.value);
                        newArgs.add(generateEncryptMethod(body, is, sc, localIndex, "lcs_for_sp_ie_"));
                        ++localIndex;
                    }else{
                        newArgs.add(v);
                    }
                }
                SpecialInvokeExpr newSpecialIE = Jimple.v().newSpecialInvokeExpr(
                        (Local)specialIE.getBase(),
                        specialIE.getMethod().makeRef(), 
                        newArgs);
                is.setInvokeExpr(newSpecialIE);
                System.out.println(" --- New SpecialIE " + newSpecialIE.toString());
            }
            
            if(ie instanceof DynamicInvokeExpr){
                DynamicInvokeExpr dynamicIE = (DynamicInvokeExpr)ie;
                for(Value v : dynamicIE.getArgs()){
                    if(v instanceof StringConstant){
                        StringConstant sc = (StringConstant)v;
                        System.out.println(" --- DynamicIS Value SC " + sc.value);
                        newArgs.add(generateEncryptMethod(body, is, sc, localIndex, "lcs_for_dyn_ie_"));
                        ++localIndex;
                    }else{
                        newArgs.add(v);
                    }
                }
                DynamicInvokeExpr newDynamicIE = Jimple.v().newDynamicInvokeExpr(
                        dynamicIE.getBootstrapMethodRef(), 
                        dynamicIE.getBootstrapArgs(),
                        dynamicIE.getMethod().makeRef(), 
                        newArgs);
                is.setInvokeExpr(newDynamicIE);
                System.out.println(" --- New DynamicIE " + newDynamicIE.toString());
            }           
        }
    }
    
    private String crypt(String s){
        
        StringBuilder str = new StringBuilder(s);
        int key = 1;
        for(int i = 0; i < str.length(); i++){
            str.setCharAt(i, Character.reverseBytes(str.charAt(i)));
            str.setCharAt(i, crypt_char_1(str.charAt(i), str.length(), key));
        }
        return str.toString();
    }
    
    public static char crypt_char_1(char ch, int str_len, int key){
        
        if(str_len < Character.MAX_VALUE){
            if(ch + str_len < Character.MAX_VALUE){
            ch = (char)(ch + str_len);
            }else{
                if(ch - str_len > 0){
                    ch = (char)(ch - str_len);
                }else{
                    ch = (char)(ch + key);
                }
            }
        }else{
            if(ch + key < Character.MAX_VALUE){
                ch = (char)(ch + key);
            }else{
                if(ch - key > 0){
                    ch = (char)(ch - key);
                }else{
                    ch = (char)(ch + key);
                }
            }
        }
        return ch;
    }
    
    
    private Local generateEncryptMethod(Body body, Stmt stmt, StringConstant sc, int localIndex, String preffix){

        String cryptedSC = crypt(sc.value);
        Local strLocal = Jimple.v().newLocal(preffix + localIndex, RefType.v("java.lang.String"));
        body.getLocals().add(strLocal);
        
        AssignStmt asLocal = Jimple.v().newAssignStmt(strLocal, StringConstant.v(cryptedSC));
        body.getUnits().insertBefore(asLocal, stmt);
        
        constructEncryptMethod(body, asLocal, localIndex, strLocal);
        
        return strLocal;
    }
    
    private void constructEncryptMethod(Body body, AssignStmt asStrLocal, int localIndex, Local strLocal){
        
        Local localStrBuilder = Jimple.v().newLocal(
                "lcs_sb_" + (localIndex + indexOffset), 
                RefType.v("java.lang.StringBuilder"));
        body.getLocals().add(localStrBuilder);
        
        NewExpr ne = Jimple.v().newNewExpr(RefType.v("java.lang.StringBuilder"));
        AssignStmt asSB = Jimple.v().newAssignStmt(localStrBuilder, ne);
        body.getUnits().insertAfter(asSB, asStrLocal);
        
//        Dup1Inst dupSB = Baf.v().newDup1Inst(RefType.v("java.lang.StringBuilder"));
//        body.getUnits().insertAfter(dupSB, asSB);
        
        SpecialInvokeExpr sieInitSB = Jimple.v().newSpecialInvokeExpr(
                localStrBuilder, 
                sbInitRef, 
                strLocal);
        InvokeStmt isInitSB = Jimple.v().newInvokeStmt(sieInitSB);
        body.getUnits().insertAfter(isInitSB, asSB);
        
        Local localCycleIndex = Jimple.v().newLocal("index", IntType.v());
        body.getLocals().add(localCycleIndex);
        
        AssignStmt asCI = Jimple.v().newAssignStmt(localCycleIndex, IntConstant.v(0));
        body.getUnits().insertAfter(asCI, isInitSB);
        
        //for        
        VirtualInvokeExpr vieSBLenght = Jimple.v().newVirtualInvokeExpr(
                        localStrBuilder, 
                        sbLenghtRef);
        Local localSBLenght = Jimple.v().newLocal(
                "lcs_sb_l_" + (localIndex + indexOffset), 
                IntType.v());
        body.getLocals().add(localSBLenght);        
        AssignStmt asSBLenghtVal = Jimple.v().newAssignStmt(localSBLenght, vieSBLenght);
        body.getUnits().insertAfter(asSBLenghtVal, asCI);
        
        //cycle body
        Local localSBChar = Jimple.v().newLocal(
                "lcs_sb_c_" + (localIndex + indexOffset), 
                CharType.v());
        body.getLocals().add(localSBChar);  
        VirtualInvokeExpr vieSBCharAt = Jimple.v().newVirtualInvokeExpr(
                        localStrBuilder, 
                        sbCharAtRef,
                        localCycleIndex);
        AssignStmt asSBCharAtResult = Jimple.v().newAssignStmt(localSBChar, vieSBCharAt);
        body.getUnits().insertAfter(asSBCharAtResult, asSBLenghtVal);
        
        Local localSBCharToInteger = Jimple.v().newLocal("lsp_sb_c_to_int_" + (localIndex + indexOffset), IntType.v());
        body.getLocals().add(localSBCharToInteger);
        
        AssignStmt asSBCharToInt = Jimple.v().newAssignStmt(localSBCharToInteger, localSBChar);
        body.getUnits().insertAfter(asSBCharToInt, asSBCharAtResult);
        
        NopStmt nop1 = Jimple.v().newNopStmt();
        body.getUnits().insertAfter(nop1, asSBCharToInt);
        
        Stmt asCharacterRB = addReverseBytes(body, nop1, localSBCharToInteger, localIndex + indexOffset);      
        addEncryptChar1(body, asSBCharToInt, nop1, localSBCharToInteger, 
                localSBLenght, localIndex + indexOffset);
  
        AssignStmt asForCast = Jimple.v().newAssignStmt(
                localSBChar,
                Jimple.v().newCastExpr(localSBCharToInteger, CharType.v())
        );
        body.getUnits().insertAfter(asForCast, asCharacterRB);
        
        ArrayList<Local> args = new ArrayList<>();
        args.add(localCycleIndex);
        args.add(localSBChar);
        
        VirtualInvokeExpr vieSBSetCharAt = Jimple.v().newVirtualInvokeExpr(
                        localStrBuilder, 
                        sbSetCharAtRef,
                        args);
        InvokeStmt isSBSetCharAt = Jimple.v().newInvokeStmt(vieSBSetCharAt);
        body.getUnits().insertAfter(isSBSetCharAt, asForCast);
              
        AddExpr incCI = Jimple.v().newAddExpr(localCycleIndex, IntConstant.v(1));
        AssignStmt asIncCI = Jimple.v().newAssignStmt(localCycleIndex, incCI);
        body.getUnits().insertAfter(asIncCI, isSBSetCharAt);
        
        VirtualInvokeExpr vieSBToStr = Jimple.v().newVirtualInvokeExpr(
                        localStrBuilder, 
                        sbToStringRef);  
        AssignStmt asSBResult = Jimple.v().newAssignStmt(strLocal, vieSBToStr);
        body.getUnits().insertAfter(asSBResult, asIncCI);
        
        GeExpr cycleCondition = Jimple.v().newGeExpr(localCycleIndex, localSBLenght);
        IfStmt cycleCondStmt = Jimple.v().newIfStmt(cycleCondition, asSBResult);
        body.getUnits().insertAfter(cycleCondStmt, asSBLenghtVal);
        
        GotoStmt gotoCycleCond = Jimple.v().newGotoStmt(cycleCondStmt);
        body.getUnits().insertAfter(gotoCycleCond, asIncCI);
    }
    
    private Stmt addReverseBytes(Body body, Stmt stmtBeforeIt, Local localSBChar, int localIndex){
        
        final int offset = 65280;
        AssignStmt asForAndExpr = Jimple.v().newAssignStmt(
                localSBChar, 
                Jimple.v().newAndExpr(localSBChar, IntConstant.v(offset)));
        body.getUnits().insertAfter(asForAndExpr, stmtBeforeIt);
        
        Local shrRes = Jimple.v().newLocal("lsp_RB_shr_res_" + (localIndex + indexOffset), IntType.v());
        body.getLocals().add(shrRes);
        AssignStmt asForShrExpr = Jimple.v().newAssignStmt(
                shrRes, 
                Jimple.v().newShrExpr(localSBChar, IntConstant.v(8))
        );
        body.getUnits().insertAfter(asForShrExpr, asForAndExpr);
        
        Local shlRes = Jimple.v().newLocal("lsp_RB_shl_res_" + (localIndex + indexOffset), IntType.v());
        body.getLocals().add(shlRes);
        AssignStmt asForShlExpr = Jimple.v().newAssignStmt(
                shlRes, 
                Jimple.v().newShlExpr(localSBChar, IntConstant.v(8))
        );
        body.getUnits().insertAfter(asForShlExpr, asForShrExpr);
        
        AssignStmt asForOrExpr = Jimple.v().newAssignStmt(
                localSBChar, 
                Jimple.v().newOrExpr(
                        shrRes, 
                        shlRes)
        );
        body.getUnits().insertAfter(asForOrExpr, asForShlExpr);

        return asForOrExpr;
    }
    
    private void addEncryptChar1(Body body, Stmt stmtBeforeIt, Stmt stmtAfterIt, 
            Local localSBChar, Local localSBLenght, int localIndex){
        
        int key = 1;
        
        AssignStmt as1 = Jimple.v().newAssignStmt(
                localSBChar, 
                Jimple.v().newAddExpr(localSBChar, localSBLenght)
        );
        body.getUnits().insertAfter(as1, stmtBeforeIt);       
        AssignStmt as2 = Jimple.v().newAssignStmt(
                localSBChar, 
                Jimple.v().newSubExpr(localSBChar, localSBLenght)
        );
        body.getUnits().insertAfter(as2, as1);       
        AssignStmt as3 = Jimple.v().newAssignStmt(
                localSBChar, 
                Jimple.v().newAddExpr(localSBChar, IntConstant.v(key))
        );
        body.getUnits().insertAfter(as3, as2);       
        AssignStmt as4 = Jimple.v().newAssignStmt(
                localSBChar, 
                Jimple.v().newSubExpr(localSBChar, IntConstant.v(key))
        );
        body.getUnits().insertAfter(as4, as3);

        //miss else block
        GotoStmt gotoEndOfEC1_1 = Jimple.v().newGotoStmt(stmtAfterIt);
        body.getUnits().insertAfter(gotoEndOfEC1_1, as1);
        
        GotoStmt gotoEndOfEC1_2 = Jimple.v().newGotoStmt(stmtAfterIt);
        body.getUnits().insertAfter(gotoEndOfEC1_2, as2);
        
        GotoStmt gotoEndOfEC1_3 = Jimple.v().newGotoStmt(stmtAfterIt);
        body.getUnits().insertAfter(gotoEndOfEC1_3, as3);
        
        Local resForCondition1 = Jimple.v().newLocal("lsp_res_cond_" + localIndex + "_1", IntType.v());
        body.getLocals().add(resForCondition1);
        AssignStmt as5 = Jimple.v().newAssignStmt(
                resForCondition1, 
                Jimple.v().newMulExpr(localSBLenght, IntConstant.v(2))
        );
        body.getUnits().insertBefore(as5, as1);
        AssignStmt as6 = Jimple.v().newAssignStmt(
                resForCondition1, 
                Jimple.v().newSubExpr(localSBChar, resForCondition1)
        );
        body.getUnits().insertAfter(as6, as5);
        IfStmt condStmt1 = Jimple.v().newIfStmt(
                Jimple.v().newGeExpr(resForCondition1, IntConstant.v(0)), 
                as2
        );
        body.getUnits().insertAfter(condStmt1, as6);
        
        Local resForCondition2 = Jimple.v().newLocal("lsp_res_cond_" + localIndex + "_2", IntType.v());
        body.getLocals().add(resForCondition2);
        AssignStmt as7 = Jimple.v().newAssignStmt(
                resForCondition2, 
                Jimple.v().newAddExpr(localSBChar, localSBLenght)
        );
        body.getUnits().insertBefore(as7, as5);
        IfStmt condStmt2 = Jimple.v().newIfStmt(
                Jimple.v().newGtExpr(resForCondition2, IntConstant.v(Character.MAX_VALUE)), 
                as2
        );
        body.getUnits().insertAfter(condStmt2, as7);   
        
        Local resForCondition3 = Jimple.v().newLocal("lsp_res_cond_" + localIndex + "_3", IntType.v());
        body.getLocals().add(resForCondition3);
        AssignStmt as8 = Jimple.v().newAssignStmt(
                resForCondition3, 
                Jimple.v().newSubExpr(localSBChar, IntConstant.v(key * 2))
        );
        body.getUnits().insertBefore(as8, as3);
        IfStmt condStmt3 = Jimple.v().newIfStmt(
                Jimple.v().newGeExpr(resForCondition3, IntConstant.v(0)), 
                as4
        );
        body.getUnits().insertAfter(condStmt3, as8);
        
        Local resForCondition4 = Jimple.v().newLocal("lsp_res_cond_" + localIndex + "_4", IntType.v());
        body.getLocals().add(resForCondition4);
        AssignStmt as9 = Jimple.v().newAssignStmt(
                resForCondition4, 
                Jimple.v().newAddExpr(localSBChar, localSBLenght)
        );
        body.getUnits().insertBefore(as9, as8);
        IfStmt condStmt4 = Jimple.v().newIfStmt(
                Jimple.v().newGtExpr(resForCondition4, IntConstant.v(Character.MAX_VALUE)), 
                as4
        );
        body.getUnits().insertAfter(condStmt4, as9); 
        
        IfStmt condStmt5 = Jimple.v().newIfStmt(
                Jimple.v().newGeExpr(localSBLenght, IntConstant.v(Character.MAX_VALUE)), 
                as9
        );
        body.getUnits().insertBefore(condStmt5, as7); 
    }
    
}
