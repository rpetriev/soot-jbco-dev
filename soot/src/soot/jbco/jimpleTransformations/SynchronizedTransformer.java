/* Soot - a J*va Optimization Framework
 * Copyright (C) 2015 Roman Petriev
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

package soot.jbco.jimpleTransformations;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import soot.Body;
import soot.BodyTransformer;
import soot.G;
import soot.IntType;
import soot.Local;
import soot.PatchingChain;
import soot.RefType;
import soot.SootMethod;
import soot.Unit;
import soot.jbco.IJbcoTransform;
import soot.jbco.util.Rand;
import soot.jimple.AssignStmt;
import soot.jimple.CaughtExceptionRef;
import soot.jimple.EnterMonitorStmt;
import soot.jimple.ExitMonitorStmt;
import soot.jimple.GeExpr;
import soot.jimple.GotoStmt;
import soot.jimple.IdentityStmt;
import soot.jimple.IfStmt;
import soot.jimple.IntConstant;
import soot.jimple.Jimple;
import soot.jimple.LookupSwitchStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.ReturnVoidStmt;
import soot.jimple.Stmt;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.toolkits.graph.UnitGraph;

/**
 * @author Roman Petriev 
 * 
 * Created on 04-Jun-2015 
 */
public class SynchronizedTransformer extends BodyTransformer implements IJbcoTransform {
    
    public static String dependancies[] = new String[] { "wjtp.jbco_fr", "bb.jbco_ful", "jtp.jbco_so" };
    public static String name = "jtp.jbco_so";
    
    private int indexOffset = 0;
    private int enterMonitorsTransformed = 0;
    private int exitMonitorsTransformed = 0;
    private int synchronizedMethodsTransformed = 0;
    
    private void init(){
        indexOffset = Rand.getInt(100);
    }
    
    @Override
    protected void internalTransform(Body body, String phaseName, Map<String, String> options) {
        
        int weight = soot.jbco.Main.getWeight(phaseName, body.getMethod().getSignature());
        if (weight == 0){ 
            return;
        }
        
        init();
        
        PatchingChain<Unit> units = body.getUnits();  
        
        if (weight > 4){
            if(body.getMethod().isSynchronized()){
                
                SootMethod method = body.getMethod();
                method.setModifiers(method.getModifiers() ^ soot.Modifier.SYNCHRONIZED);
                
                for (Unit u : units) {
                    if (u instanceof IdentityStmt)
                        continue;
                    
                    // This the first real statement. If it is not a MonitorEnter
                    // instruction, we generate one
                    if (!(u instanceof EnterMonitorStmt)) {
                        body.getUnits().insertBeforeNoRedirect(Jimple.v().newEnterMonitorStmt(body.getThisLocal()), u);

                        // We also need to leave the monitor when the method terminates
                        UnitGraph graph = new ExceptionalUnitGraph(body);
                        for (Unit tail : graph.getTails())
                            body.getUnits().insertBefore(Jimple.v().newExitMonitorStmt(body.getThisLocal()), tail);
                    }
                    break;
                }
                
                Stmt lastReturn = null;
                Stmt realFirst = null;
                for (Unit u : units) {
                    Stmt stmt = (Stmt)u;
                    if (realFirst == null && !(stmt instanceof IdentityStmt))
                        realFirst = stmt;
                    
                    if(stmt instanceof ReturnStmt || stmt instanceof ReturnVoidStmt){
                        lastReturn = stmt;
                    }
                }
                
                if(lastReturn != null && realFirst != null){
                    
                    GotoStmt gotoForCatch = Jimple.v().newGotoStmt(lastReturn);
                    units.insertAfter(gotoForCatch, units.getPredOf(lastReturn)); 

                    RefType throwable = G.v().soot_Scene().getRefType("java.lang.Throwable");
                    CaughtExceptionRef cexc = Jimple.v().newCaughtExceptionRef();
                    Local excLocal = Jimple.v().newLocal("Throwable_CaughtExceptionLocal", throwable);
                    body.getLocals().add(excLocal);
                    
                    Unit handler = Jimple.v().newIdentityStmt(excLocal,cexc);
                    units.insertAfter(handler, gotoForCatch);
                    ExitMonitorStmt ems = Jimple.v().newExitMonitorStmt(body.getThisLocal());
                    units.insertAfter(ems, handler);
                    units.insertAfter(Jimple.v().newThrowStmt(excLocal), ems);
                    body.getTraps().add(Jimple.v().newTrap(throwable.getSootClass(), realFirst, lastReturn, handler));
                }

                synchronizedMethodsTransformed++;
            }
        }
        
        Iterator stmtIt = units.snapshotIterator();
        while(stmtIt.hasNext()){      
            Stmt stmt = (Stmt)stmtIt.next();
            if(stmt instanceof EnterMonitorStmt){

                obfuscateStmt(body, units, stmt);   
                enterMonitorsTransformed++;
            }
            
            if(weight > 6){
                if(stmt instanceof ExitMonitorStmt){

                    obfuscateStmt(body, units, stmt);   
                    exitMonitorsTransformed++;
                }
            }
        }
        
    }

    @Override
    public void outputSummary() {
        out.println("\"Entermonitors\" transformed: " + enterMonitorsTransformed);
        out.println("\"Exitmonitors\" transformed: " + exitMonitorsTransformed);
        out.println("Methods transformed: " + synchronizedMethodsTransformed);
    }

    @Override
    public String[] getDependancies() {
        return dependancies;
    }

    @Override
    public String getName() {
        return name;
    }
    
    private void obfuscateStmt(Body body, PatchingChain<Unit> units, Stmt stmt){
        
        int localIndex = 0;
        Local intLocal1 = Jimple.v().newLocal("myInt" + (indexOffset + localIndex), IntType.v());
        body.getLocals().add(intLocal1);
        localIndex++;
        Local intLocal2 = Jimple.v().newLocal("myInt" + (indexOffset + localIndex), IntType.v());
        body.getLocals().add(intLocal2);
        AssignStmt asLocal1 = Jimple.v().newAssignStmt(intLocal1, IntConstant.v(500));
        units.insertBefore(asLocal1, stmt);
        AssignStmt asLocal2 = Jimple.v().newAssignStmt(intLocal2, IntConstant.v(500));
        units.insertAfter(asLocal2, asLocal1);

        units.remove(stmt);
        units.add(stmt);

        AssignStmt asAddStmt = Jimple.v().newAssignStmt(intLocal1, Jimple.v().newAddExpr(intLocal1, intLocal2));
        units.insertAfter(asAddStmt, asLocal2);

        GeExpr condition = Jimple.v().newGeExpr(intLocal1, intLocal2);
        IfStmt ifForGoto = Jimple.v().newIfStmt(condition, stmt);
        units.add(ifForGoto);

        ArrayList<IntConstant> values = new ArrayList<IntConstant>();
        ArrayList<Unit> targets = new ArrayList<Unit>();

        values.add(IntConstant.v(Rand.getInt(200)));
        GotoStmt gtst = Jimple.v().newGotoStmt(asAddStmt);
        units.add(gtst);
        targets.add(gtst);
        LookupSwitchStmt switchForJumpToMonitor = Jimple.v().newLookupSwitchStmt(intLocal1, values, targets, ifForGoto);
        units.insertBefore(switchForJumpToMonitor, asAddStmt);

        AssignStmt asAddStmtForGoto = Jimple.v().newAssignStmt(intLocal1, Jimple.v().newAddExpr(intLocal1, intLocal2));
        units.insertAfter(asAddStmtForGoto, switchForJumpToMonitor);
        GeExpr condition2 = Jimple.v().newGeExpr(intLocal1, intLocal2);
        IfStmt ifForGoto2 = Jimple.v().newIfStmt(condition2, asAddStmtForGoto);
        units.insertAfter(ifForGoto2, stmt);
        AssignStmt asAddStmtForEndOfCode = Jimple.v().newAssignStmt(intLocal1, Jimple.v().newAddExpr(intLocal1, intLocal2));
        units.insertAfter(asAddStmtForEndOfCode, ifForGoto2);
        indexOffset += localIndex;
    }
       
}
